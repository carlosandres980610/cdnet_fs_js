import app from './app';
const http = require('http');
const server = http.createServer(app);
const port = process.env.APP_PORT;

async function main() {
    server.listen(port);
}

main();

export default app;
