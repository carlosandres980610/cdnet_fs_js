import mongoose from 'mongoose';

export default class DB {
    async connect() {
        try {
            await mongoose.connect(String(process.env.DB));
            console.log("Correct DB connection");
        } catch (error) { 
            console.log(error);
        }
    };

    async disconnect() {
        try {
            await mongoose.disconnect();
        } catch (error) {
        }
    }
}
