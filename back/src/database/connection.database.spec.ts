import DB from './connection.database';

describe('DBConnection suite', () => {

    const db = new DB;
    it('DB Open and Close', async () => {
        await db.connect();
        await db.disconnect();

    });

});
