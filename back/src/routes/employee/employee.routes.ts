import router from 'express';
import { EmployeeController } from '../../controllers/employee/employee.controller';

const employeeRoutes = router();
const employeeController = new EmployeeController();

employeeRoutes.post('/addEmployee', employeeController.addEmployee);
employeeRoutes.get('/getEmployees', employeeController.getEmployee);
employeeRoutes.get('/getEmployee/:id', employeeController.getSingleEmployee);
employeeRoutes.post('/updateEmployee', employeeController.updateEmployee);


export default employeeRoutes;
