import express from 'express';
import { Response, Request, NextFunction } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import DB from './database/connection.database';
import dotenv from 'dotenv';
import path from 'path';
import employeeRoutes from './routes/employee/employee.routes';

class App {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.appDotenv();
        this.appMiddlewares();
        this.appRoutes();
        this.appCors();
        this.defaultRoute();
        this.databaseConnection();
        this.appRoutes();
    }
    appDotenv() {
        dotenv.config({
            path: path.resolve(process.cwd(), process.env.NODE_ENV + '.env'),
        });
    }

    appMiddlewares() {
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use('/public', express.static('public'));
        this.app.use(bodyParser.json());
        this.app.use(morgan('dev'));
    }

    appCors() {
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept, Authorization',
            );
            if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods',
                    'POST, GET');
                return res.status(200).json({});
            }
            next();
        });
    }

    defaultRoute() {
        this.app.get('/',
            (_req: Request, res: Response, next: NextFunction) => {
                res.status(200).json({
                    message: 'CdNet backend',
                });
            });
    }

    databaseConnection() {
        const db = new DB();
        db.connect();
    }

    appRoutes() {
        this.app.use(cors());
        this.app.use('/api/employee', employeeRoutes);
    }
}

export default new App().app;
