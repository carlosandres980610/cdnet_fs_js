import mongoose from 'mongoose';
import { Document } from 'mongoose';

export interface IEmployee extends Document {
    _id: string;
    name: string;
    secondName: string;
    lastName: string;
    secondLastName: string;
    country: string;
    email: string;
    idType: string;
    idNumber: string;
    ingress: Date;
    area: string;
};

const employee = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true,
        min: 2,
        max: 20,
        match: /[A-Z ]+$/
    },
    secondName: {
        type: String,
        min: 2,
        max: 50,
        match: /[A-Z ]+$/
    },
    lastName: {
        type: String,
        required: true,
        min: 2,
        max: 20,
        match: /[A-Z ]+$/
    },
    secondLastName: {
        type: String,
        required: true,
        min: 2,
        max: 20,
        match: /[A-Z ]+$/
    },
    country: {
        type: String,
        enum: ["COL", "USA"]
    },
    email: {
        type: String,
        unique: true,
        required: true,
        // eslint-disable-next-line max-len
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
        max: 300,
        min: 10
    },
    idType: {
        type: String,
        enum: ["CC", "CE", "P", "PE"],
        required: true,
        default: "CC"
    },
    idNumber: {
        type: String,
        min: 1,
        max: 20,
        required: true,
        match: /[A-Za-z0-9]+$/
    },
    ingress: {
        type: Date,
    },
    area: {
        type: String,
        enum: ["Administración", "Financiera", "Compras", "Infraestructura", "Operación", "Talento Humano", "Servicios Varios"]
    },
    status: {
        type: String,
        default: "Active",
        enum: ["Active", "Inactive"]
    }
});

employee.index({ idType: 1, idNumber: 1 }, { unique: true });
employee.set('timestamps', true);
export default mongoose.model<IEmployee>('Employee', employee);
