import mongoose from 'mongoose';
import Employee from '../../models/employee.model';
import { Request, Response } from 'express';

export interface Employee extends mongoose.Document {
    name: string,
    email: string,
    password: number,
}

export class EmployeeController {
    public async addEmployee(req: Request, res: Response) {
        const emailClass = new EmailClass();
        try {
            var date = new Date();
            date.setMonth(date.getMonth() - 1);
            var ingressDate = new Date(req.body.ingress)
            let email = await emailClass.emailGenerator(req.body.name, req.body.lastName, req.body.country);

            if (ingressDate.getTime() < date.getTime()) {
                return res.status(500).json({
                    result: {
                        success: false,
                        message: 'invalidIngressDate',
                    },
                    data: undefined,
                });
            }

            const employee = new Employee({
                _id: new mongoose.Types.ObjectId(),
                name: req.body.name.toUpperCase(),
                secondLastName: req.body.secondLastName.toUpperCase(),
                secondName: req.body.secondName.toUpperCase(),
                lastName: req.body.lastName.toUpperCase(),
                email: email,
                idType: req.body.idType,
                idNumber: req.body.idNumber,
                ingress: ingressDate,
                area: req.body.area,
                country: req.body.country
            });

            await employee.save()
                .then((_rest) => {
                    return res.status(200).json({
                        result: {
                            success: true,
                            message: 'success',
                        },
                        data: undefined,
                    });
                }).catch((err) => {
                    return res.status(500).json({
                        result: {
                            success: false,
                            message: 'failed',
                            error: err
                        },
                        data: undefined,
                    });
                });

        } catch (ex) {
            return res.status(500).json({
                result: {
                    success: false,
                    message: 'exception',
                    error: ex
                },
                data: undefined,
            });
        }
    }

    public async getEmployee(req: Request, res: Response) {
        await Employee.find({})
            .exec()
            .then((result) => {
                return res.status(200).json({
                    result: {
                        success: true,
                        message: 'success',
                    },
                    data: result,
                });
            }).catch((err) => {
                return res.status(500).json({
                    result: {
                        success: false,
                        message: 'failed',
                        error: err
                    },
                    data: undefined,
                });
            });
    }


    public async getSingleEmployee(req: Request, res: Response) {
        await Employee.findOne({ _id: req.params.id })
            .exec()
            .then((result) => {
                return res.status(200).json({
                    result: {
                        success: true,
                        message: 'success',
                    },
                    data: result,
                });
            }).catch((err) => {
                return res.status(500).json({
                    result: {
                        success: false,
                        message: 'failed',
                        error: err
                    },
                    data: undefined,
                });
            });
    }


    public async updateEmployee(req: Request, res: Response) {
        const emailClass = new EmailClass();
        let email = req.body.email;

        await Employee.findOne({ _id: req.body._id })
            .exec()
            .then(async (result) => {
                if (result?.name !== req.body.name || result?.lastName !== req.body.lastName) {
                    email = await emailClass.emailGenerator(req.body.name, req.body.lastName, req.body.country);
                }


                const employee = new Employee({
                    name: req.body.name.toUpperCase(),
                    secondLastName: req.body.secondLastName.toUpperCase(),
                    secondName: req.body.secondName.toUpperCase(),
                    lastName: req.body.lastName.toUpperCase(),
                    idType: req.body.idType,
                    idNumber: req.body.idNumber,
                    ingress: req.body.ingress,
                    area: req.body.area,
                    status: req.body.status,
                    country: req.body.country,
                    email: email
                });

                await Employee.findOneAndUpdate({ _id: req.body._id }, { $set: employee }, { new: true })
                    .exec()
                    .then((_rest) => {
                        return res.status(200).json({
                            result: {
                                success: true,
                                message: 'success',
                            },
                            data: undefined,
                        });
                    }).catch((err) => {
                        return res.status(500).json({
                            result: {
                                success: true,
                                message: 'failed',
                                error: err
                            },
                            data: undefined,
                        });
                    });


            }).catch((error) => {
                return res.status(500).json({
                    result: {
                        success: false,
                        message: 'failed',
                        error: error
                    },
                    data: undefined,
                });
            });
    }



}


export class EmailClass {
    public async emailGenerator(name: string, lastname: string, country: string) {
        try {
            let emailName = name.toLowerCase().replace(/ /g, '') + "." + lastname.toLowerCase().replace(/ /g, '');
            let emailDomain = "@cidenet.com." + (country === "COL" ? "co" : "us")
            let email = emailName + emailDomain;

            await Employee.find({ email: email })
                .then((result) => {
                    if (result.length > 0) {
                        email = emailName + "." + result.length + emailDomain;
                    }
                });
            return email;

        } catch (ex) {
            return "";
        }

    }
}