import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction) => {

    try {
        const token = req.headers.authorization?.split(' ')[1];
        jwt.verify(String(token), String(process.env.JWT_KEY));
        next();
    } catch (error) {
        res.status(401).json({
            result: {
                state: 14,
                error: 'No authorized ',
                success: false,
            },
        });
    }
};
