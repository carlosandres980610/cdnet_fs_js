// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const APP_API = 'http://localhost:3000/api/employee';

export const environment = {
  production: false,
  getEmployees: APP_API + '/getEmployees',
  getEmployee: APP_API + '/getEmployee',
  addEmployee: APP_API + '/addEmployee',
  updateEmployee: APP_API + '/updateEmployee',


};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
