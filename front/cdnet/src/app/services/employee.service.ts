import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Employee, EmployeeResponse, EmployeesResponse } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  constructor(public http: HttpClient) { }

  getEmployees(): Observable<EmployeesResponse> {
    return this.http.get<EmployeesResponse>(environment.getEmployees);
  }

  getSingleEmployee(id: string): Observable<EmployeeResponse> {
    return this.http.get<EmployeeResponse>(environment.getEmployee + '/' + id);
  }

  addEmployee(body: Employee): Observable<EmployeeResponse> {
    return this.http.post<EmployeeResponse>(environment.addEmployee, body);
  }

  updateEmployee(body: Employee): Observable<EmployeeResponse> {
    return this.http.post<EmployeeResponse>(environment.updateEmployee, body);
  }
}
