import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  form: FormGroup;
  title: string = "Añadir empleado";
  id: string = "";
  isEdit = false;
  constructor(private employeeService: EmployeeService, private router: Router, private fb: FormBuilder, private activeRoute: ActivatedRoute) {
    this.validateParam();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.maxLength(20), Validators.minLength(2), Validators.required, Validators.pattern(/[A-Z ]+$/)]],
      secondName: ['', [Validators.maxLength(50), Validators.minLength(2), Validators.pattern(/[A-Z ]+$/)]],
      lastName: ['', [Validators.maxLength(20), Validators.minLength(2), Validators.required, Validators.pattern(/[A-Z ]+$/)]],
      secondLastName: ['', [Validators.maxLength(20), Validators.minLength(2), Validators.required, Validators.pattern(/[A-Z ]+$/)]],
      area: ['', [Validators.required]],
      status: ['', [Validators.required]],
      country: ['', [Validators.required]],
      ingress: ['', [Validators.required]],
      idType: ['', [Validators.required]],
      email: ['', [Validators.required]],
      idNumber: ['', [Validators.maxLength(20), Validators.minLength(2), Validators.required, Validators.pattern(/[A-Za-z0-9]+$/)]],
    })
  }

  validateParam() {
    this.id = this.activeRoute.snapshot.paramMap.get('id');
    if (this.id != undefined) {
      this.title = "Editar empleado";
      this.setUpdateData(this.id);
    }
  }

  editEmployee() {
    const body: Employee = {
      name: this.form.get('name').value,
      secondName: this.form.get('name').value,
      lastName: this.form.get('lastName').value,
      secondLastName: this.form.get('secondLastName').value,
      area: this.form.get('area').value,
      status: this.form.get('status').value,
      country: this.form.get('country').value,
      email: this.form.get('email').value,
      ingress: this.form.get('ingress').value,
      idType: this.form.get('idType').value,
      idNumber: this.form.get('idNumber').value

    }

    if (this.isEdit) {
      body._id = this.id;
      this.employeeService.updateEmployee(body).subscribe((res) => {
        if (res.result.success) {
          this.router.navigate(["/employees"]);

        }
      });
    } else {
      this.employeeService.addEmployee(body).subscribe((res) => {
        if (res.result.success) {
          this.router.navigate(["/employees"]);

        }
      });
    }

  }

  setUpdateData(id: string) {
    this.isEdit = true;
    this.employeeService.getSingleEmployee(id).subscribe((res) => {

      this.form.patchValue({
        name: res.data.name,
        secondName: res.data.secondName,
        lastName: res.data.lastName,
        secondLastName: res.data.secondLastName,
        area: res.data.area,
        ingress: this.formatDate(res.data.ingress),
        status: res.data.status,
        idType: res.data.idType,
        idNumber: res.data.idNumber,
        country: res.data.country,
        email: res.data.email
      });
    });

  }

  private formatDate(date) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
}
