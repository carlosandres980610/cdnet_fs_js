import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Employee } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit, AfterViewInit {

  employeeQueryList: Employee[] = [];
  displayedColumns: string[] = ['name', 'secondName', 'lastName', 'secondLastName', 'email', 'area', 'status', 'ingress', 'edition', 'actions'];
  dataSource = new MatTableDataSource<Employee>();

  constructor(private employeeService: EmployeeService, private router: Router) {
    this.employeeService.getEmployees().subscribe((res) => {
      this.employeeQueryList = res.data;
      this.dataSource = new MatTableDataSource<Employee>(res.data);
    });
  }

  ngOnInit(): void {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  addEmployee() {
    this.router.navigate(['/addEmployee']);
  }

  goEdit(id: string) {
    this.router.navigate(['/updateEmployee', id]);

  }

}
