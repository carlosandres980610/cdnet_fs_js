import { Result } from '../models/general.model';

export class Employee {
  _id?: string;
  name: string;
  secondName: string;
  lastName: string;
  secondLastName: string;
  email: string;
  country: string;
  ingress: Date;
  area: string;
  status: string;
  idType: string;
  idNumber: string;

  constructor(
    _id: string,
    name: string,
    secondName: string,
    lastName: string,
    secondLastName: string,
    email: string,
    country: string,
    ingress: Date,
    area: string,
    status: string
  ) {
    this._id = _id;
    this.name = name;
    this.secondName = secondName;
    this.lastName = lastName;
    this.secondLastName = secondLastName;
    this.email = email;
    this.country = country;
    this.ingress = ingress;
    this.area = area;
    this.status = status;
  }
}


export class EmployeesResponse {
  result: Result;
  data: Employee[];

  constructor(result: Result, data: Employee[]) {
    this.result = result;
    this.data = data;
  }
}

export class EmployeeResponse {
  result: Result;
  data: Employee;

  constructor(result: Result, data: Employee) {
    this.result = result;
    this.data = data;
  }
}
