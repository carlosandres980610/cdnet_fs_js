export class Result {
  success: boolean;
  error?: string;
  message?: string;
}

export class GeneralResult {
  result: Result;
  data: any;
}
